repo --name=Main	--baseurl=http://abf-downloads.rosalinux.ru/rosa2019.1/repository/x86_64/main/release
repo --name=Main-Up	--baseurl=http://abf-downloads.rosalinux.ru/rosa2019.1/repository/x86_64/main/updates

repo --name=Contrib	--baseurl=http://abf-downloads.rosalinux.ru/rosa2019.1/repository/x86_64/contrib/release
repo --name=Contrib-Up	--baseurl=http://abf-downloads.rosalinux.ru/rosa2019.1/repository/x86_64/contrib/updates

repo --name=Non-Free	--baseurl=http://abf-downloads.rosalinux.ru/rosa2019.1/repository/x86_64/non-free/release
repo --name=Non-Free-Up	--baseurl=http://abf-downloads.rosalinux.ru/rosa2019.1/repository/x86_64/non-free/updates

repo --name=Restricted	--baseurl=http://abf-downloads.rosalinux.ru/rosa2019.1/repository/x86_64/restricted/release
repo --name=Restricted-Up	--baseurl=http://abf-downloads.rosalinux.ru/rosa2019.1/repository/x86_64/restricted/updates

repo --name=kernel-xanmod	--baseurl=http://abf-downloads.rosalinux.ru/kelpee_personal/container/3708998/x86_64/main/release/
