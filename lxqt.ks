auth --useshadow --enablemd5
selinux --disabled
# TODO: include firewalld
firewall --disabled
firstboot --enabled

# Enable/Disable services
services --enabled=sddm,avahi-daemon,cpupower,sshd,cups,wsdd,systemd-resolved,NetworkManager
#services --disabled=nmb,smb

# partition
part / --size 12268 --fstype ext4

# configure Time Zone
timezone --utc Europe/Moscow

# configure language
lang ru_RU.UTF-8 --addsupport=en_US

# configure keyboard
keyboard en

# repository
%include repobase.ks

%packages --nocore
%include packages.ks
%end

%post

### Manually (re)generate initrds
# TODO: move its generation from %%post to %%postrrans in kernel
# packages, and then all needed dracut modules will be already
# installed during inird generation, and there will be no need
# to (re)generate initrds here.
# BUT dracut config by livecd-tools is partly broken
# https://github.com/livecd-tools/livecd-tools/issues/158
# We remove odd parts - modules that do not exist in this kernel.
# Another possible approach is making and using our own config from scratch.
C="${C:-/etc/dracut.conf.d/99-liveos.conf}"
find /boot -name 'vmlinuz-*' | sort -u | while read -r line ; do
	kver="$(echo "$line" | sed -e 's,^/boot/vmlinuz-,,g')"
	cp "${C}" "${C}.orig"
	to_find="$(cat "$C" | grep -E '^(filesystems\+=|add_drivers\+=)' | sed -e 's,",,g' | awk -F '+=' '{print $NF}' | tr ' ' '\n' | grep -v '=' | tr '\n' ' ' | sed -e 's,  , ,g')"
	not_exist_list=""
	for i in ${to_find} ; do
		if ! find "/lib/modules/${kver}" -name "${i}.ko*" | grep -q '.' ; then
			not_exist_list="${not_exist_list} ${i}"
		fi
	done
	sed -i -e 's,+=",+=" ,g' "$C"
	for i in ${not_exist_list} ; do
		sed -i -E -e "s,[[:blank:]]${i}[[:blank:]], ,g" "$C"
	done
	sed -i -e 's,  , ,g' "$C"
	diff -u "${C}.orig" "${C}" || :
	rm -f "${C}.orig"
	dracut -f "/boot/initrd-${kver}.img" "${kver}"
done
find /boot -name 'initrd-*' -print

### Use for LXQt

# Disable the issue of trusting the installer LXQt
/bin/mkdir -p /etc/skel/.local/share/gvfs-metadata
/bin/echo \
'00000000: da1a 6d65 7461 0100 0000 0000 d9ff 9b77  ..meta.........w
00000010: 0000 0030 0000 0020 0000 0000 6045 2b4d  ...0... ....`E+M
00000020: 0000 0001 0000 0028 7472 7573 7400 0000  .......(trust...
00000030: 0000 0040 0000 0044 0000 0090 a1e4 2571  ...@...D......%q
00000040: 2f00 0000 0000 0001 0000 0058 0000 0060  /..........X...`
00000050: 0000 0094 0000 0001 4465 736b 746f 7000  ........Desktop.
00000060: 0000 0001 0000 0074 0000 008c 0000 0098  .......t........
00000070: 0000 05cd 616e 6163 6f6e 6461 2d72 6f73  ....anaconda-ros
00000080: 612e 6465 736b 746f 7000 0000 0000 0000  a.desktop.......
00000090: 0000 0000 0000 0000 0000 0001 0000 0000  ................
000000a0: 0000 00a4 7472 7565 0000 0000            ....true....' \
	|/usr/bin/xxd -r > /etc/skel/.local/share/gvfs-metadata/home

if [ -f /usr/share/applications/liveinst.desktop ]; then
	cat >> /etc/anaconda-scripts.d/livecd-init/11-lxqt.sh << EOF
#!/bin/bash
# Desktop icon
mv -f /usr/share/applications/liveinst.desktop /usr/share/applications/anaconda-rosa.desktop
sed -i 's!NoDisplay=true!NoDisplay=false!' /usr/share/applications/anaconda-rosa.desktop
sed -i 's!Name=.*!Name=Install Rosa LXQt\nName[ru]=Установить Rosa LXQt!' /usr/share/applications/anaconda-rosa.desktop
sed -i 's!Icon=.*!Icon=anaconda-rosa!' /usr/share/applications/anaconda-rosa.desktop
ln -s anaconda-rosa.desktop /usr/share/applications/anaconda.desktop
cp -f /usr/share/applications/anaconda-rosa.desktop /home/live/Desktop/anaconda-rosa.desktop
EOF
chmod +x /etc/anaconda-scripts.d/livecd-init/11-lxqt.sh

mkdir -p /etc/anaconda-scripts.d/post-install
cat >> /etc/anaconda-scripts.d/post-install/11-lxqt.sh << EOF
#!/bin/bash
# Artifact from live
rm -f /etc/skel/.local/share/gvfs-metadata/home
EOF
chmod +x /etc/anaconda-scripts.d/post-install/11-lxqt.sh
fi

### End of use for LXQt

echo "#BUILD_ID#" > $INSTALL_ROOT/etc/isonumber
echo "# iso build No.#BUILD_ID#" > $LIVE_ROOT/rpm.lst
rpm --root $INSTALL_ROOT -qa | sort >> $LIVE_ROOT/rpm.lst
rpm --root $INSTALL_ROOT -qa --queryformat="%{NAME}\n" | sort >> $LIVE_ROOT/rpm_names.lst
rpm --root $INSTALL_ROOT -qa --queryformat="%{NAME}\n" | sort > $INSTALL_ROOT/var/lib/rpm/installed-by-default
rpm --root $INSTALL_ROOT -qa --queryformat="%{SOURCERPM}\n" | sort > $INSTALL_ROOT/var/lib/rpm/sourcerpms

%end

# debug
%post --nochroot
ls -la $LIVE_ROOT/isolinux/
cp -v "$(find "${INSTALL_ROOT}/boot" -name 'initrd-*.img' | sort -u | tail -n 1)" "${LIVE_ROOT}/isolinux/initrd0.img"
%end

