auth --useshadow --enablemd5
selinux --disabled
# TODO: include firewalld
firewall --disabled
firstboot --enabled

# Enable/Disable services
services --enabled=sddm,avahi-daemon,cpupower,sshd,cups,wsdd,systemd-resolved,NetworkManager
#services --disabled=nmb,smb

# partition
part / --size 12268 --fstype ext4

# configure Time Zone
timezone --utc Europe/Moscow

# configure language
lang ru_RU.UTF-8 --addsupport=en_US

# configure keyboard
keyboard en

# repository
%include repobase.ks

%packages --nocore
%include packages.ks
%end

%post

### Manually (re)generate initrds
# TODO: move its generation from %%post to %%postrrans in kernel
# packages, and then all needed dracut modules will be already
# installed during inird generation, and there will be no need
# to (re)generate initrds here.
# BUT dracut config by livecd-tools is partly broken
# https://github.com/livecd-tools/livecd-tools/issues/158
# We remove odd parts - modules that do not exist in this kernel.
# Another possible approach is making and using our own config from scratch.
C="${C:-/etc/dracut.conf.d/99-liveos.conf}"
find /boot -name 'vmlinuz-*' | sort -u | while read -r line ; do
	kver="$(echo "$line" | sed -e 's,^/boot/vmlinuz-,,g')"
	cp "${C}" "${C}.orig"
	to_find="$(cat "$C" | grep -E '^(filesystems\+=|add_drivers\+=)' | sed -e 's,",,g' | awk -F '+=' '{print $NF}' | tr ' ' '\n' | grep -v '=' | tr '\n' ' ' | sed -e 's,  , ,g')"
	not_exist_list=""
	for i in ${to_find} ; do
		if ! find "/lib/modules/${kver}" -name "${i}.ko*" | grep -q '.' ; then
			not_exist_list="${not_exist_list} ${i}"
		fi
	done
	sed -i -e 's,+=",+=" ,g' "$C"
	for i in ${not_exist_list} ; do
		sed -i -E -e "s,[[:blank:]]${i}[[:blank:]], ,g" "$C"
	done
	sed -i -e 's,  , ,g' "$C"
	diff -u "${C}.orig" "${C}" || :
	rm -f "${C}.orig"
	dracut -f "/boot/initrd-${kver}.img" "${kver}"
done
find /boot -name 'initrd-*' -print

echo "#BUILD_ID#" > $INSTALL_ROOT/etc/isonumber
echo "# iso build No.#BUILD_ID#" > $LIVE_ROOT/rpm.lst
rpm --root $INSTALL_ROOT -qa | sort >> $LIVE_ROOT/rpm.lst
rpm --root $INSTALL_ROOT -qa --queryformat="%{NAME}\n" | sort >> $LIVE_ROOT/rpm_names.lst
rpm --root $INSTALL_ROOT -qa --queryformat="%{NAME}\n" | sort > $INSTALL_ROOT/var/lib/rpm/installed-by-default
rpm --root $INSTALL_ROOT -qa --queryformat="%{SOURCERPM}\n" | sort > $INSTALL_ROOT/var/lib/rpm/sourcerpms

%end

# debug
%post --nochroot
ls -la $LIVE_ROOT/isolinux/
cp -v "$(find "${INSTALL_ROOT}/boot" -name 'initrd-*.img' | sort -u | tail -n 1)" "${LIVE_ROOT}/isolinux/initrd0.img"
%end

