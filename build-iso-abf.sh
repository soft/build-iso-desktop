#!/bin/sh

set -x
set -e
set -u
set +f

BUILD_ID="${BUILD_ID:-XXX}"
DE="${DE:-Gnome}"
ARCH="${ARCH:-x86_64}"
FORCE_RECOMMENDS="${FORCE_RECOMMENDS:-0}"
FILTR_PACKAGES="${FILTR_PACKAGES:-0}"
ADD_PACKAGES="${ADD_PACKAGES:-0}"
ADD_REPOS="${ADD_REPOS:-0}"

FS_LABEL="ROSA_2021.1_${DE}_${BUILD_ID}"
if ! [ "$ARCH" = "x86_64" ]; then FS_LABEL="${DE}_${ARCH}_${BUILD_ID}"; fi

if [ "$DE" = "Gnome" ] || [ "$DE" = "GNOME" ] || [ "$DE" = "gnome" ]; then de=gnome; fi
if [ "$DE" = "LXQt" ] || [ "$DE" = "LXQT" ] || [ "$DE" = "lxqt" ]; then de=lxqt; fi
if [ "$DE" = "Plasma5" ] || [ "$DE" = "PLASMA5" ] || [ "$DE" = "plasma5" ]; then de=plasma5; fi
if [ "$DE" = "Xfce" ] || [ "$DE" = "XFCE" ] || [ "$DE" = "xfce" ]; then de=xfce; fi

conf="$(mktemp)"
cp ${de}.ks "$conf"
repobase="$(mktemp)"
cp repobase.ks "$repobase"
pkgs="$(mktemp)"
echo "task-iso-$de" > "$pkgs"
echo "task-iso-common" >> "$pkgs"

if [ "$FORCE_RECOMMENDS" = "1" ]; then
	dnf rq --recommends "task-iso-$de" >> "$pkgs"
	dnf rq --recommends "task-iso-common" >> "$pkgs"
fi

if ! [ "$ADD_PACKAGES" = "0" ]; then echo "$ADD_PACKAGES" |sed "s/^\| \|,\|;/\n/g" >> "$pkgs"; fi
if ! [ "$FILTR_PACKAGES" = "0" ]; then echo "$FILTR_PACKAGES" |sed "s/^\| \|,\|;/\n-/g" >> "$pkgs"; fi
if ! [ "$ADD_REPOS" = "0" ]; then echo "$ADD_REPOS" |sed "s/^\| \|,\|;/\nrepo --name=repo$RANDOM --baseurl=\n/g" >> "$repobase"; fi

if ! [ "$ARCH" = "x86_64" ]; then
	sed -i "/aarch64/d" "$repobase"
fi

if [ "$ARCH" = "i686" ]; then
	sed -i "/rosa2019.1/ s/x86_64/i686/" "$repobase"
	sed -i "/x86_64/d" "$repobase"
	sed -i "/aarch64/d" "$repobase"
	sed -i "s/^lib64/lib/" "$pkgs"
fi

if [ "$ARCH" = "aarch64" ]; then
	sed -i "/rosa2019.1/ s/x86_64/aarch64/" "$repobase"
	sed -i "/x86_64/d" "$repobase"
	sed -i "/i686/d" "$repobase"
	echo "distro-release-res" >> "$pkgs"
	echo "shim-unsigned" >> "$pkgs"
	echo "open-vm-tools" >> "$pkgs"
fi

sed -i "s,repobase.ks,${repobase},g" "$conf"
sed -i "s,packages.ks,${pkgs},g" "$conf"

dnf distrosync -y

dnf install -y		\
	coreutils	\
	findutils	\
	lsof		\
	sed		\
	tar		\
	util-linux	\
	/usr/bin/livecd-creator

mkdir -p /home/vagrant/results

livecd-creator --verbose	\
	--compression-type=xz	\
	--config="$conf"	\
	--fslabel="$FS_LABEL"

mv -v *.iso /home/vagrant/results/
